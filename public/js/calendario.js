$(document).ready(function(){

	var date = new Date();

  	//console.log(date.getDay()+" - "+date.getMonth()+" - "+date.getFullYear());
	imprimirCabecera(date);
	imprimirCalendario(date);

	$("table").on("click","td.diesNum",function(){
		console.log($(this).text());
		//$(this).attr("href","../../hora.php");
  	});
	$(".diesNum").hover(function(){
		$(this).css("background-color", "#FF9B21");
  	},function(){
		$(this).css("background-color", "white")	
	});
  

  	$("#pinta").click(function(){
    	var fecha = Module().fechaNueva(date);
    	imprimirCabecera(fecha);
    	imprimirCalendario(fecha);
 		$("table").on("click","td.diesNum",function(){
			console.log($(this).text());
			
  		});
		$(".diesNum").hover(function(){
			$(this).css("background-color", "#FF9B21");
	  	},function(){
			$(this).css("background-color", "white")	
		});

  	});

    //boton que muestra el mes anterior
    $("#anterior").click(function(){
      var mesDate = Module().mesAnterior(date);
      imprimirCabecera(mesDate);
      imprimirCalendario(mesDate);
		$("table").on("click","td.diesNum",function(){
			console.log($(this).text());
			
  		});
		$(".diesNum").hover(function(){
			$(this).css("background-color", "#FF9B21");
  		},function(){
			$(this).css("background-color", "white")	
		});
    });
    //boton que muestra el mes siguiente
    $("#siguiente").click(function(){
      var mesDate = Module().mesSiguiente(date);
      imprimirCabecera(mesDate);
      imprimirCalendario(mesDate);
 		$("table").on("click","td.diesNum",function(){
			console.log($(this).text());
		
  		});
		$(".diesNum").hover(function(){
			$(this).css("background-color", "#FF9B21");
  		},function(){
			$(this).css("background-color", "white")	
		});
    });
});

var Module = (function(){
  var primeraFila = function(mes,any){
      var dateFila = new Date(any,mes,1);
      //se resta 1 al dia para que coincida con el calendario exacto.
      var diaInicial = dateFila.getDay()-1;
      return diaInicial;
  };
  var any = function(date){
    var any = date.getFullYear();
    return any;
  }
  var diasSemana = function(){
    var dias=["LUN","MAR","MIE","JUE","VIE","SAB","DOM"];
    return dias;
  }
  var diasMes = function(date){
    //se suma 1 al mes para coincidir con el calendario
    var dia = new Date(date.getFullYear(), date.getMonth()+1,0).getDate();
    return dia;
  };
  var mesAny = function(){
    var meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    return meses;
  }
  var nuevaFecha = function(date){
    var dia = $("#dia").val();
    var mes = $("#mes").val();
    var any = $("#any").val();

    date.setMonth(mes-1);
    date.setFullYear(any);
    return date;
  }
  var anterior = function(date){
      var mes = date.getMonth();
      var any = date.getFullYear();
      mes--;
      if(mes<0){
        mes = 11;
        any--;
      }
      date.setMonth(mes);
      date.setFullYear(any);
      return date;

  }
  var siguiente = function(date){
    var mes = date.getMonth();
    var any = date.getFullYear();

    mes++;
    if(mes > 11){
      mes = 0;
      any++;
    }
    date.setMonth(mes);
    date.setFullYear(any);
    return date;

  }
  var mesPrueba = function(date){
    var mes = date.getMonth();
    var any = date.getFullYear();
    if(mes>11){
      mes =0;
      any++;
    }
    if (mes < 0) {
      mes = 11;
      any++;
    }
    date.setMonth(mes);
    date.setFullYear(any);
    return date;
  }
  var letraMes = function(date){
    //hace coincidir el mes con sus dias al restar 1
    // mes = date.getMonth()-1;
    // date.setMonth(mes);
    // date = mesPrueba(date);
    var mesTxt = Module().mesesDelAny()[date.getMonth()];
    // console.log(mes +" - "+date.getMonth());

    return mesTxt;
  }
  return {
    fechaNueva: nuevaFecha,
    mesComprueba: mesPrueba,
    anyText : any,
    filaOne : primeraFila,
    semana : diasSemana,
    diasDelMes: diasMes,
    mesesDelAny: mesAny,
    mesAnterior: anterior,
    mesSiguiente: siguiente,
    mesLetra : letraMes
  };
});

function calendario(date){

    var mes = date.getMonth();
    var any = date.getFullYear();

    var ultimoDia = Module().diasDelMes(date);


    console.log(ultimoDia);
    var primerDia = new Date(date.getFullYear(), date.getMonth(),1).getDate();
    var diaSemana = new Date(date.getFullYear(), date.getMonth(),1).getDay();

    console.log(primerDia+" - "+diaSemana);
    // var primerDia = new Date(d.getFullYear(), d.getMonth(),1).getDate();
    // var ultimoDia = new Date(d.getFullYear(), d.getMonth(),0).getDate();

    var num = 1;
    var start = 0;
    var diasString = Module().semana();
    var diasNum=[1,2,3,4,5,6,0];

    var calendarioString = "";
        calendarioString += '<table class="table">';
        // document.write('<table class="table table-striped table-inverse">');
        // document.write("<tr><td COLSPAN=4>tabla del "+num+"</td></tr>");
        for (var i = 0; i < 6; i++) {
          calendarioString += "<tr>";
          // document.write("<tr>");
          for (var j = 0; j < 7; j++) {
            if(i==0){
              calendarioString += "<td>"+diasString[j]+"</td>";
              // document.write("<td>"+dias[j]+"</td>");
            }
            else{
              if((start == 0) && (j<Module().filaOne(mes,any))){
                calendarioString+="<td></td>";
              }

              else{
                start=1;
                if(num <= ultimoDia){

                    calendarioString += "<td class='diesNum'><a href='../hora.php?d="+num+"&m="+mes+"&a="+any+"'>"+num+"</a></td>";
                    // document.write("<td>"+num+"</td>");
                    num++
                  }
              }
            }
          }
          calendarioString += "</tr>";
          // document.write("</tr>");
        }
        calendarioString += "</table>";
        // document.write("</table>");
        // console.log(calendarioString);
      return calendarioString;

  }
function cabecera(date){

    console.log(mesText);

}
 function imprimirCalendario(date){
    var cal = calendario(date);
    $("#calendario").html(cal);
 }
function imprimirCabecera(date){
  var cabe = cabecera(date);
  var any = Module().anyText(date);
  var mesText = Module().mesLetra(date);
  $("#mesText").html(mesText+" - "+any);
}
