$(document).ready(function(){

    var error;
    var errorEmail;

    $("#matricula").focusout(function(){
        if($(this).val().match(/^[0-9]{4}[BCDFGHJKLMNPRSTVWXYZ]{3}$/)){
		console.log("matricula correcta");
		error = 0;
		$(this).css('border-color','#808080');
        } else {
            error=1;
            $(this).css('border-color', '#FF3333');
		console.log("Matricula incorrecta");
        }
    });

    $("#matricula").keyup(function(){
	if($(this).val().match(/^[0-9]{4}[BCDFGHJKLMNPRSTVWXYZ]{3}$/)){
		error = 0;
		console.log("Matricula correcta");
		$(this).css('border-color','#808080');
    	}else {
		error = 1;
		console.log("Matricula incorrecta");
		$(this).css('border-color','#FF3333');
	}

    });

    $("#entrarMat").click(function(event){
        if(error == 1){
            event.preventDefault();
            alert("La matricula és incorrecta.");

        }
    });

    //Validar email 

    $("#email").focusout(function(){
        if($(this).val().match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)){
		errorEmail = 0;
		$(this).css('border-color','#808080');
        } else if($(this).val() == ""){
		errorEmail = 0;
		$(this).css('border-color','#808080');
	} else{
            $(this).css('border-color', '#FF3333');
            errorEmail = 1;
        }
    });


   $("#email").keyup(function(){
	if($(this).val().match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)){
		console.log("email correcte");
		errorEmail = 0;
		$(this).css('border-color','#808080');
	} else if ($(this).val() == ""){
		errorEmail = 0;
		$(this).css('border-color','#808080');
	} else {
		console.log("email incorrecte");
		errorEmail = 1;
		$(this).css('border-color','#FF3333');
	}

   });

    $("#submit").click(function(event){
        if(error == 1 || errorEmail == 1){
            event.preventDefault();
            alert("Les dades introduïdes són incorrectes.Revisar-les");
        } else {
		guardar();
	}
    });

});


function guardar(){
	var resposta = confirm("Estàs segur que vols aquesta cita?\nNo es pot modificar després.");

		        if (resposta == true){
                		window.location = "controladors/demanaCita.php";
                		console.log("Sí, cita guardada");
        		} else {
                		window.location = "index.php";
                		console.log("No, cita no guardada");

			}
}

