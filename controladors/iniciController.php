<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>itv</title>
<link rel="stylesheet" type="text/css" href="../public/css/index.css">
<link rel="stylesheet" type="text/css" href="../public/css/calendari.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">


<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>

</head>
<body>
  <div class="container" align="center">


    <img id="logo" src="../public/img/IAM_CAT_logos_purple_rgb_3.png" height="300" width="1200">


    <h1>MOTORS AUSIAS MARCH</h1>
<?php
	session_start();

    $matri = $_POST['matricula'];
    $_SESSION['matri'] = $matri;
	$var = 1;
	include('../models/dbModel.php');

	$arrayCotxes = select("cotxe");
	$arrayClients = select("client");

	foreach ($arrayCotxes as $objetoCotxe) {

    	foreach ($objetoCotxe as $atributo) {
			if($matri == $atributo){

				$_SESSION["cotxeArray"] = $objetoCotxe;
				$var *= 0;
			}
			else{
				$var *= 1;
			}
    	}

  	 }
		$idClient = $_SESSION["cotxeArray"][1];

	foreach ($arrayClients as $objetoClient) {

    	foreach ($objetoClient as $atributo) {
			if($idClient == $atributo){

				$_SESSION["clientArray"] = $objetoClient;

			}

    	}

  	 }
	//foreach($_SESSION["clientArray"] as $clientObject){
		//echo($clientObject."<br>");
	//}
	if ($var==0){
		//echo "tu cotxe esta registrado <br>";
		mostraCitaView();
	}
	else{
		//echo "tu cotxe no esta en el sistema <br>";
		demanaCitaView();
	}
	function mostraCitaView(){
		require '../cita.php';
		//echo '<a href="../cita.php">ver cita</a>';
	}
	function demanaCitaView(){
		require '../calendari.php';

		//echo '<a href="../calendari.php">demana cita</a>';
	}
?>

 </div>
 	<script type="text/javascript" src="../public/js/calendario.js"></script>
</body>
</html>
