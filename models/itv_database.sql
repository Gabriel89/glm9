
DROP DATABASE IF EXISTS itv_database;

CREATE DATABASE itv_database;

USE itv_database;

DROP TABLE IF EXISTS admin;

create table admin(
	idAdmin INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	usuario varchar(20) NOT NULL,
	password varchar(20) NOT NULL
)ENGINE = INNODB;

DROP TABLE IF EXISTS client;

create table client(
	idClient int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nombre varchar(20) NOT NULL,
	telefono varchar(15),
	email varchar(50)
)ENGINE = INNODB;

DROP TABLE IF EXISTS cotxe;
CREATE TABLE cotxe(
	idCotxe int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idClient int NOT NULL,
	matricula varchar(7) NOT NULL,
	model varchar(50),
	dataITV DATE,
	horaITV TIME,
	FOREIGN KEY (idClient) REFERENCES client(idClient) ON DELETE CASCADE
)ENGINE = INNODB;

DROP TABLE IF EXISTS taller_ausias;
CREATE TABLE taller_ausias(
	idTaller INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idCotxe INT NOT NULL,
	dataAusias DATE,
	linea1 TIME,
	linea2 TIME,
	FOREIGN KEY (idCotxe) REFERENCES cotxe(idCotxe) ON DELETE CASCADE
)ENGINE = INNODB;

/*El format per dataITV és YYYY-MM-DD
  El format per horaITV eś HH:MM:SS
*/

INSERT INTO client (nombre,telefono,email) VALUES
("Silvia","789 643 345","silvia@iam.cat"),
("Alexzy","456 742 234",NULL),
("Dennis","567 454 432",NULL),
("Gabriel","789 643 345", "gabriel@iam.cat"),
("link","847 263 920","link@iam.cat"),
("Carlos","247 720 453", "carlos@iam.cat"),
("Luis","987 456 201", "luis@iam.cat"),
("Criss","456 018 453", "criss@iam.cat"),
("lycka","945 288 453", "lycka@iam.cat"),
("Paloma","920 432 453", "paloma@iam.cat");

INSERT INTO cotxe (idClient,matricula,model,dataITV,horaITV) VALUES
(1,"1234JKH","BMW A8","2017-12-11","08:00"),
(2,"3454DCB","Ford 2017","2017-12-11","08:00"),
(3,"6782YGH","Mini Cooper 2010","2017-12-05","15:00"),
(4,"4353LVY","Opel Mocha","2017-12-20","18:30"),
(5,"1234GTB","Seat 484","2017-12-20","14:30"),
(6,"1724LJB","Lamborgini","2017-12-11","09:30"),
(7,"1984MDB","Mercedes","2017-12-11","09:30"),
(8,"1724FDB","Ferrari","2017-12-11","10:00"),
(9,"1724WRB","volkswagen","2017-12-11","10:30"),
(10,"1304TRB","Toyota","2017-12-11","9:00");

INSERT INTO taller_ausias (idCotxe,dataAusias,linea1,linea2) VALUES
(2,"2017-12-11","08:00",null),
(5,"2017-12-11",null,"08:00"),
(1,"2017-12-05","15:00",null),
(4,"2017-12-20","18:30",null),
(3,"2017-12-20","14:30",null),
(6,"2017-12-11","9:30",null),
(7,"2017-12-11",null,"9:30"),
(8,"2017-12-11","10:00",null),
(9,"2017-12-11",null,"10:30"),
(10,"2017-12-11","9:00",null);

/*DELETE client, cotxe FROM client INNER JOIN cotxe ON cotxe.idClient = client.idClient WHERE cotxe.matricula = "2345TNG"*/
/*SELECT  * FROM client C INNER JOIN cotxe T ON C.idClient = T.idClient AND T.matricula = '3454DCA';*/
/*SELECT  * FROM cotxe C INNER JOIN taller_ausias T ON C.idCotxe = T.idCotxe AND C.matricula = '3454DCA';*/
/*DELETE client, cotxe , taller_ausias FROM client INNER JOIN cotxe INNER JOIN taller_ausias WHERE client.idClient = cotxe.idClient AND cotxe.idCotxe = taller_ausias.idCotxe AND matricula='6782YGH';*/
